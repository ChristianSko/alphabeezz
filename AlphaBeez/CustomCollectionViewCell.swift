//
//  CustomCollectionViewCell.swift
//  AlphaBeez
//
//  Created by Ivan Tilev on 12/05/2020.
//  Copyright © 2020 Ivan Tilev. All rights reserved.
//
// This is the custom class that will hold the image property of the cell
import UIKit

class CustomCollectionViewCell: UICollectionViewCell {
    
    // Outlets
    @IBOutlet weak var imageView: UIImageView!
}
